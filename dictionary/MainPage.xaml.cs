﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Plugin.Connectivity;

namespace dictionary
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Handle_Completed(object sender, System.EventArgs e)
        {
            if (!(DoIHaveInternet()))
            {
                await DisplayAlert("Error", "There is no internet connection", "Okay");
            }
            else
            {
                var word = ((Entry)sender).Text;

                word = word.ToLower();

                if (word == "")
                {
                    //await DisplayAlert("Error", "Word cannot be empty.", "Okay");
                    myLabel.Text = "Entry cannot be empty.";
                }
                else
                {
                    myLabel.Text = "";
                    var client = new HttpClient();
                    var address = "https://owlbot.info/api/v2/dictionary/" + word;
                    var uri = new Uri(address);

                    List<WordDef> data = new List<WordDef>();

                    var response = await client.GetAsync(uri);

                    if (response.IsSuccessStatusCode)
                    {
                        var jsonContent = await response.Content.ReadAsStringAsync();
                        data = JsonConvert.DeserializeObject<List<WordDef>>(jsonContent);

                        if (data.Count == 0)
                        {
                            //await DisplayAlert("Error", "That is not a word. Try again.", "Okay");
                            myLabel.Text = "That is not a word. Try again.";
                        }
                    }


                    myList1.ItemsSource = new ObservableCollection<WordDef>(data);
                }
            }
        }

        public bool DoIHaveInternet()
        {
            return CrossConnectivity.Current.IsConnected;
        }
    }
}