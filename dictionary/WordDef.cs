﻿using System;
using System.Collections.Generic;
namespace dictionary
{
   /*public class Definitions
    {
        public List<WordDef> Defins { get; set; }
    }*/
    public class WordDef
    {
        public string type { get; set; }
        public string definition { get; set; }
        public string example { get; set; }
    }
}
